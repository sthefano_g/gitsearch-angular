export class RequestTypes {
  login?: string;
  name?: string;
  avatar_url?: string;
  score?: number;
  url?: string;
  created_at?: any;
  followers?: number;
  following?: number;
  html_url?: string;
  items?: any;
  total_count?: number;
}

export class SearchForm {
  searchInput?: string;
}
export class ContactResponse {
  isValid?: boolean;
  errors?: Array<string>;
}
