import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { RequestTypes } from './Requests';

@Injectable({
  providedIn: 'root',
})
export class RequestsService {
  private apiUrl = 'https://api.github.com/';

  constructor(private http: HttpClient) {}

  getUsers(username: string): Observable<RequestTypes> {
    return this.http.get<RequestTypes>(
      this.apiUrl + 'search/users?q=' + username
    );
  }

  getFullUser(url: string): Observable<RequestTypes> {
    return this.http.get(url);
  }
}
