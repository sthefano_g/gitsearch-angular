import { Component, Input, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RequestTypes } from 'src/app/services/Requests/Requests';
import { RequestsService } from 'src/app/services/Requests/requests.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
})
export class ModalComponent implements OnInit {
  data: string;
  @Input() url!: string;
  @Input() user!: RequestTypes;

  constructor(
    private requestService: RequestsService,
    @Inject(MAT_DIALOG_DATA) data: any
  ) {
    this.data = data.url;
  }

  ngOnInit(): void {
    this.requestUser(this.url);
  }

  requestUser(url: string) {
    this.requestService.getFullUser(this.data).subscribe({
      next: (data: RequestTypes) => {
        this.user = data;
      },
      error: (error) => {
        console.error('Deu ruim', error.message);
      },
    });
  }
}
