import { Component, Input, OnInit } from '@angular/core';
import { RequestTypes } from 'src/app/services/Requests/Requests';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ModalComponent } from '../../modal/modal.component';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})
export class CardComponent implements OnInit {
  @Input() userList?: RequestTypes;
  @Input() searchValue!: string;

  constructor(private dialog: MatDialog) {}

  ngOnInit(): void {}

  openNewTab(url: string) {
    window.open(url);
  }

  openDialog(url: string) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = url;
    this.dialog.open(ModalComponent, {
      width: '65%',
      data: { url: url },
    });
  }
}
