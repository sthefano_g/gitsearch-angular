import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  @Input() text: string = 'Feito por: Sthéfano Garcia - 0x/04/2022'

  constructor() { }

  ngOnInit(): void {
  }

}
