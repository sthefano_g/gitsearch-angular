import { Component, OnInit, Input } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { RequestTypes } from 'src/app/services/Requests/Requests';
import { RequestsService } from 'src/app/services/Requests/requests.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  form!: FormGroup;
  userFound: boolean = true;

  @Input() userList!: RequestTypes;
  @Input() searchValue!: string;

  constructor(
    private fb: FormBuilder,
    private requestService: RequestsService
  ) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      searchInput: ['', Validators.required],
    });
  }

  get f(): { [key: string]: AbstractControl } {
    return this.form?.controls;
  }

  sendInput() {
    let searchInput = this.f['searchInput'].value;

    this.searchValue = this.f['searchInput'].value;

    this.requestService.getUsers(searchInput).subscribe({
      next: (data: RequestTypes) => {
        this.userList = data;
      },
      error: (error) => {
        console.error('Deu ruim', error.message);
      },
      complete: () => {
        if (this.userList.total_count == 0) {
          this.userFound = false;
        } else {
          this.userFound = true;
        }
      },
    });
  }
}
